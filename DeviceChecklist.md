Device Checklist
================

This Checklist is a supplement for all porters to be able to inform users in detail about their progress, as well as track their readiness for being added as a community or core device.

For either qualifying as a community or a core device a corresponding feature set from the list below needs to be confirmed working. The details will be published soon here.

For the convenience of your users present this list in the following way:

Working
-------

Working with additional steps
-----------------------------

Not working
-----------

Put all working features in under the first headline. Put features where the user needs to to additonal (manual) steps under the second headline. Everything else goes under the third headline.

* Actors: Manual brightness
* Actors: Notification LED
* Actors: Torchlight
* Actors: Vibration
* Camera: Flashlight
* Camera: Photo
* Camera: Video
* Cellular: Carrier info, signal strength
* Cellular: Data connection
* Cellular: Incoming, outgoing calls
* Cellular: MMS in, out
* Cellular: PIN unlock
* Cellular: SMS in, out
* Cellular: Change audio routings
* Cellular: Voice in calls
* Endurance: Battery lifetime > 24h from 100%
* Endurance: No reboot needed for 1 week
* GPU: Boot into UI
* GPU: Video acceleration
* Misc: Anbox patches applied to kernel
* Misc: Battery percentage
* Misc: Offline charging
* Misc: Online charging
* Misc: Wirelss charging - only for devices that support it
* Misc: Recovery image
* Misc: Reset to factory defaults
* Misc: Shutdown / Reboot
* Network: Bluetooth
* Network: Flight mode
* Network: Hotspot
* (Network: NFC - disabled atm due to no middleware)
* Network: WiFi
* Sensors: Automatic brightness
* (Sensors: Fingerprint reader - disabled atm due to no middleware)
* Sensors: GPS
* Sensors: Proximity
* Sensors: Rotation
* Sensors: Touchscreen
* Sound: Earphones
* Sound: Loudspeaker
* Sound: Microphone
* Sound: Volume control
* USB: MTP access
* USB: RNDIS access
* USB: External monitor - only for devices that support it
